namespace Sertifi;

public interface IGetStudentsHttpClient
{
    Task<List<GetStudentModel>> GetAllStudents();
}