using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Sertifi;

public class SatisfiCompetencyService : ISatisfiCompetencyService
{
    private readonly IGetStudentsHttpClient _getStudentsHttpClient;
    private readonly IStudentsProcessor _studentsProcessor;
    private readonly IPutStudentsAggregateHttpClient _putStudentsAggregateHttpClient;

    public SatisfiCompetencyService(
        IGetStudentsHttpClient getStudentsHttpClient,
        IStudentsProcessor studentsProcessor,
        IPutStudentsAggregateHttpClient putStudentsAggregateHttpClient
        )
    {
        _getStudentsHttpClient = getStudentsHttpClient;
        _studentsProcessor = studentsProcessor;
        _putStudentsAggregateHttpClient = putStudentsAggregateHttpClient;
    }

    public async Task Execute()
    {
        var students = await _getStudentsHttpClient.GetAllStudents();
        var processedStudents = _studentsProcessor.CreateStudentAggregate(students);
        Console.WriteLine(JsonConvert.SerializeObject(
            processedStudents, Formatting.Indented,
            new JsonConverter[] {new StringEnumConverter()}));
        await _putStudentsAggregateHttpClient.PutStudentsAggregate(processedStudents);
        Console.ReadLine();
    }
}