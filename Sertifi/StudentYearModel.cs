namespace Sertifi;

public class StudentYearModel
{
    public StudentYearModel(int year, double gpa, int studentId)
    {
        Year = year;
        Gpa = gpa;
        StudentId = studentId;
    }

    public int Year { get; }
    public double Gpa { get; }
    public int StudentId { get; }
}