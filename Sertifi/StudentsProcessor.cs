using System.Collections.Generic;
using System.Linq;
namespace Sertifi;

public class StudentsProcessor : IStudentsProcessor
{
    public PutStudentAggregateModel CreateStudentAggregate(IList<GetStudentModel> students)
    {
        PutStudentAggregateModel result = new PutStudentAggregateModel();
        result.YourName = "Scott Bush";
        result.YourEmail = "bushscott@icloud.com";
        PopulateDynamicStudentAggregateData(students, result);
        return result;
    }

    private void PopulateDynamicStudentAggregateData(IList<GetStudentModel> students, PutStudentAggregateModel result)
    {
        //you can argue that iterations over the whole collection multiple times is inefficient but the readability is much better.
        //so i went with less efficient since our data input size is small
        //We could optimize this to do all these calculations in one loop but it will get more complicated and out input would
        //have to increase by 10000x to see a noticeable improvement
        result.StudentIdMostInconsistent =
            students.MaxBy(x => x.GPARecord.Max() - x.GPARecord.Min()).Id;
        result.Top10StudentIdsWithHighestGpa =
            students.OrderByDescending(x => x.GPARecord.Average())
                    .Take(10)
                    .Select(x => x.Id)
                    .ToList();
        PopulateYearlyAggregateData(students, result);
    }

    private void PopulateYearlyAggregateData(IList<GetStudentModel> students, PutStudentAggregateModel result)
    {
        List<StudentYearModel> studentYearModels = new List<StudentYearModel>();
        foreach (var student in students)
        {
            for (int i = 0; i <= student.EndYear - student.StartYear; i++)
            {
                studentYearModels.Add(
                    new StudentYearModel(
                        student.StartYear + i,
                        student.GPARecord[i],
                        student.Id));
            }
        }

        result.YearWithHighestAttendance =
            studentYearModels
                .GroupBy(x => x.Year)
                .OrderByDescending(y => y.Count())
                .FirstOrDefault()
                .Key;

        result.YearWithHighestOverallGpa =
            studentYearModels
                .GroupBy(x => x.Year)
                .OrderByDescending(
                    y => y.Average(z => z.Gpa))
                .FirstOrDefault().Key;
    }
}