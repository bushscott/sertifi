using System.Text;
using Newtonsoft.Json;

namespace Sertifi;

public class PutStudentsAggregateHttpClient : IPutStudentsAggregateHttpClient
{
    private readonly HttpClient _httpClient;

    public PutStudentsAggregateHttpClient(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task PutStudentsAggregate(PutStudentAggregateModel putStudentAggregateModel)
    {
        var objAsJson = JsonConvert.SerializeObject(putStudentAggregateModel);
        var content = new StringContent(objAsJson, Encoding.UTF8, "application/json");
        await _httpClient.PutAsync("http://apitest.sertifi.net/api/StudentAggregate", content);
    }
}