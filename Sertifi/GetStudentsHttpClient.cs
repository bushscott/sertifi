using Newtonsoft.Json;

namespace Sertifi;

public class GetStudentsHttpClient : IGetStudentsHttpClient
{
    private readonly HttpClient _httpClient;

    public GetStudentsHttpClient(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<List<GetStudentModel>> GetAllStudents()
    {
        var responseString = await _httpClient.GetStringAsync("http://apitest.sertifi.net/api/Students");

        var students = JsonConvert.DeserializeObject<List<GetStudentModel>>(responseString);
        if (students != null)
        {
            return students;
        }
        throw new Exception("GetStudentsHttpClient.GetAllStudents failed.");
    }
}