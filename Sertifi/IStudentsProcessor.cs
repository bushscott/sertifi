namespace Sertifi;

public interface IStudentsProcessor
{
    PutStudentAggregateModel CreateStudentAggregate(IList<GetStudentModel> students);
}