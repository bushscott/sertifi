namespace Sertifi;

public interface ISatisfiCompetencyService
{
    Task Execute();
}