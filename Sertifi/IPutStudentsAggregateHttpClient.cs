namespace Sertifi;

public interface IPutStudentsAggregateHttpClient
{
    Task PutStudentsAggregate(PutStudentAggregateModel putStudentAggregateModel);
}