﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sertifi;

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((_, services) =>
        services
            .AddTransient<IStudentsProcessor, StudentsProcessor>()
            .AddTransient<ISatisfiCompetencyService, SatisfiCompetencyService>()
            .AddHttpClient<IGetStudentsHttpClient, GetStudentsHttpClient>()
            .AddTypedClient<IPutStudentsAggregateHttpClient, PutStudentsAggregateHttpClient>()
        )
    .Build();
    
await Execute(host.Services);

static async Task Execute(IServiceProvider services)
{
    using IServiceScope serviceScope = services.CreateScope();
    IServiceProvider provider = serviceScope.ServiceProvider;

    ISatisfiCompetencyService satisfiCompetencyService = provider.GetRequiredService<ISatisfiCompetencyService>();
    await satisfiCompetencyService.Execute();
}