using Newtonsoft.Json;

namespace Sertifi;

public class GetStudentModel
{
    [JsonProperty("Id")]
    public int Id { get; set; }

    [JsonProperty("Name")]
    public string Name { get; set; }

    [JsonProperty("StartYear")]
    public int StartYear { get; set; }

    [JsonProperty("EndYear")]
    public int EndYear { get; set; }

    [JsonProperty("GPARecord")]
    public IReadOnlyList<double> GPARecord { get; set; }
}