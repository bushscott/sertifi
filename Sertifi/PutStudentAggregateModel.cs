using Newtonsoft.Json;

namespace Sertifi;

public class PutStudentAggregateModel
{
    [JsonProperty("YourName")]
    public string YourName { get; set; }

    [JsonProperty("YourEmail")]
    public string YourEmail { get; set; }

    [JsonProperty("YearWithHighestAttendance")]
    public int YearWithHighestAttendance { get; set; }

    [JsonProperty("YearWithHighestOverallGpa")]
    public int YearWithHighestOverallGpa { get; set; }

    [JsonProperty("Top10StudentIdsWithHighestGpa")]
    public List<int> Top10StudentIdsWithHighestGpa { get; set; }

    [JsonProperty("StudentIdMostInconsistent")]
    public int StudentIdMostInconsistent { get; set; }
}