using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Sertifi;

namespace sertifiTestts;
//had to use moq for httpclient. fakeiteasy is used for the rest
[TestClass]
public class GetStudentsHttpClientTests
{
    private string apiReturnedString = @"[{""Id"":1,""Name"":""Jack"",""StartYear"":2013,""EndYear"":2016,""GPARecord"":[3.4,2.1,1.2,3.6]},{""Id"":2,""Name"":""Jill"",""StartYear"":2010,""EndYear"":2013,""GPARecord"":[3.3,2.3,1.1,3.7]}]";
    private HttpClient moqdHttpClient = null;

    [TestMethod]
    public async Task testGetAllStudentsReturnsTwoStudentObjects()
    {
        //arrange
        var handlerMock = new Mock<HttpMessageHandler>();
        handlerMock
            .Protected()
            .Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>()
            )
            .ReturnsAsync(new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(apiReturnedString)
            })
            .Verifiable();
        moqdHttpClient = new HttpClient(handlerMock.Object);
        IGetStudentsHttpClient getStudentsHttpClient = new GetStudentsHttpClient(moqdHttpClient);
        
        //act
        var students = await getStudentsHttpClient.GetAllStudents();
        
        //Assert
        Assert.AreEqual(2, students.Count);
    }
    
    [TestMethod]
    public async Task testGetAllStudentsReturnJackAndJill()
    {
        //arrange
        var handlerMock = new Mock<HttpMessageHandler>();
        handlerMock
            .Protected()
            .Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>()
            )
            .ReturnsAsync(new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(apiReturnedString)
            })
            .Verifiable();
        moqdHttpClient = new HttpClient(handlerMock.Object);
        IGetStudentsHttpClient getStudentsHttpClient = new GetStudentsHttpClient(moqdHttpClient);
        
        //act
        var students = await getStudentsHttpClient.GetAllStudents();
        
        //Assert
        Assert.IsTrue(students.Any(x => x.Name == "Jack"));
        Assert.IsTrue(students.Any(x => x.Name == "Jill"));
    }
}