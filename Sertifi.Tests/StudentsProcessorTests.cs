using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sertifi;

namespace sertifiTestts;

[TestClass]
public class StudentsProcessorTests
{
    private readonly IList<GetStudentModel> _sudentModels =
        new List<GetStudentModel>()
        {
            new GetStudentModel()
            {
                Id = 1,
                Name = "Jack",
                StartYear = 2013,
                EndYear = 2016,
                GPARecord = new []
                {
                    3.4,2.1,1.2,3.6
                }
            },
            new GetStudentModel()
            {
                Id = 2,
                Name = "Jill",
                StartYear = 2010,
                EndYear = 2013,
                GPARecord = new []
                {
                    3.3,2.3,1.1,3.7
                }
            },
            new GetStudentModel()
            {
            Id = 3,
            Name = "Bob",
            StartYear = 2010,
            EndYear = 2012,
            GPARecord = new []
                {
                    2.3,2.5,2.8
                }
            },
            new GetStudentModel()
            {
                Id = 4,
                Name = "Alice",
                StartYear = 2013,
                EndYear = 2016,
                GPARecord = new []
                {
                    3.6,2.9,3.4,3.7
                }
            },
            new GetStudentModel()
            {
                Id = 5,
                Name = "Eve",
                StartYear = 2012,
                EndYear = 2015,
                GPARecord = new []
                {
                    3.3,2.5,1.1,3.7
                }
            },
            new GetStudentModel()
            {
                Id = 6,
                Name = "Malcom",
                StartYear = 2011,
                EndYear = 2014,
                GPARecord = new []
                {
                    3.8,2.7,1.1,3.7
                }
            },
            new GetStudentModel()
            {
                Id = 7,
                Name = "Don",
                StartYear = 2009,
                EndYear = 2012,
                GPARecord = new []
                {
                    3.1,2.1,1.1,3.7
                }
            },
            new GetStudentModel()
            {
                Id = 8,
                Name = "Mike",
                StartYear = 2009,
                EndYear = 2011,
                GPARecord = new []
                {
                    3.6,2.2,1.1
                }
            },
            new GetStudentModel()
            {
                Id = 9,
                Name = "Stacy",
                StartYear = 2015,
                EndYear = 2016,
                GPARecord = new []
                {
                    3.3,2.3
                }
            },
            new GetStudentModel()
            {
                Id = 10,
                Name = "Safron",
                StartYear = 2016,
                EndYear = 2016,
                GPARecord = new []
                {
                    3.3
                }
            },
            new GetStudentModel()
            {
                Id = 11,
                Name = "Bill",
                StartYear = 2012,
                EndYear = 2015,
                GPARecord = new []
                {
                    3.6,2.4,2.3,3.7
                }
            },
        };
    [TestMethod]
    public void testCreateStudentAggregateReturnsYourName()
    {
        //arange
        IStudentsProcessor studentsProcessor = A.Fake<StudentsProcessor>();
        //act
        var studentAggregate = studentsProcessor.CreateStudentAggregate(_sudentModels);
        //assert
        Assert.IsTrue(studentAggregate.YourName == "Scott Bush");
    }
    
    [TestMethod]
    public void testCreateStudentAggregateReturnsYourEmail()
    {
        //arange
        IStudentsProcessor studentsProcessor = A.Fake<StudentsProcessor>();
        //act
        var studentAggregate = studentsProcessor.CreateStudentAggregate(_sudentModels);
        //assert
        Assert.IsTrue(studentAggregate.YourEmail == "bushscott@icloud.com");
    }
    
    [TestMethod]
    public void testCreateStudentAggregateReturnsYearWithHighestAttendance2013()
    {
        //arange
        IStudentsProcessor studentsProcessor = A.Fake<StudentsProcessor>();
        //act
        var studentAggregate = studentsProcessor.CreateStudentAggregate(_sudentModels);
        //assert
        Assert.IsTrue(studentAggregate.YearWithHighestAttendance == 2013);
    }
    
    [TestMethod]
    public void testCreateStudentAggregateReturnsYearWithHighestOverallGpa2009()
    {
        //arange
        IStudentsProcessor studentsProcessor = A.Fake<StudentsProcessor>();
        //act
        var studentAggregate = studentsProcessor.CreateStudentAggregate(_sudentModels);
        //assert
        Assert.IsTrue(studentAggregate.YearWithHighestOverallGpa == 2009);
    }
    
    [TestMethod]
    public void testCreateStudentAggregateReturnsStudentIdMostInconsistent6()
    {
        //arange
        IStudentsProcessor studentsProcessor = A.Fake<StudentsProcessor>();
        //act
        var studentAggregate = studentsProcessor.CreateStudentAggregate(_sudentModels);
        //assert
        Assert.IsTrue(studentAggregate.StudentIdMostInconsistent == 6);
    }
    
    [TestMethod]
    public void testCreateStudentAggregateReturnsTop10StudentIdsWithHighestGpaCount10()
    {
        //arange
        IStudentsProcessor studentsProcessor = A.Fake<StudentsProcessor>();
        //act
        var studentAggregate = studentsProcessor.CreateStudentAggregate(_sudentModels);
        //assert
        Assert.IsTrue(studentAggregate.Top10StudentIdsWithHighestGpa.Count == 10);
    }
    
    [TestMethod]
    public void testCreateStudentAggregateReturnsTop10StudentIdsWithHighestGpaNotId8()
    {
        //arange
        IStudentsProcessor studentsProcessor = A.Fake<StudentsProcessor>();
        //act
        var studentAggregate = studentsProcessor.CreateStudentAggregate(_sudentModels);
        //assert
        Assert.IsTrue(!studentAggregate.Top10StudentIdsWithHighestGpa.Contains(8));
    }
}